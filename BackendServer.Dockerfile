FROM node:14

WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn install

COPY . .

RUN yarn tsc
RUN yarn tscpaths -p tsconfig.json -s ./src -o ./dist

EXPOSE 8080
EXPOSE 2222
CMD [ "node", "./dist/server/server.js" ]