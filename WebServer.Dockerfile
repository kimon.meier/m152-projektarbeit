FROM nimmis/alpine-apache
RUN apk add nodejs npm yarn g++ gcc libgcc libstdc++ linux-headers make python3 git fts-dev

WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn install

COPY . .

RUN yarn parcel build ./src/client/index.html
RUN mkdir /web/html/
RUN cp ./dist/*.* /web/html/

EXPOSE 80
EXPOSE 443