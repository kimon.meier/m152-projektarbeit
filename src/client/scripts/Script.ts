import MessageSentEntity from "@shared/entities/MessageSentEntity";
import imageCompression from "browser-image-compression";
import TypingIndication from "../../shared/entities/TypingIndicator";
import { ClientEvents } from "../../shared/enums/ClientEvents";
import { ServerEvents } from "../../shared/enums/ServerEvents";
import { ServerMessage } from "../../shared/message/ServerMessage";
import StringHelper from "../../shared/utils/StringUtils";
import WebSocketClient from "../connection/WebSocketClient";

class Script {
    private readonly MessageTemplate = "<div class=\"row chatmessage\"><div class=\"col-4 col-md-3 col-lg-2 col-xl-1 chatname\">{{name}}:</div><div class=\"col-5 col-sm-6 col-md-7 col-lg-10 chatcontent\">{{message}} {{image}}</div><div class=\"col-3 col-sm-2 col-lg-1 chatname\">{{time}}</div></div>" 
    private readonly MemberTemplate = " <div id=\"member-{{uuid}}\" class=\"row member\"> <div class=\"col-10\">{{name}}</div> <div class=\"dotnet-plulse\"> </div></div>";
    private readonly ImageTemplate = "<img style='height: 100%; width: 100%; object-fit: contain' src=\"{{dataUrl}}\"></img>";

    private static Instance: Script;

    private loginDiv!: HTMLDivElement;
    private pageDiv!: HTMLDivElement;

    private webSocket!: WebSocketClient;

    public start(): void {
        Script.Instance = this;

        window.onresize = () => {
            if(window.innerWidth > 768) {
                document.getElementById("memberSection")?.classList.remove("row");
                document.getElementById("chatSection")?.classList.remove("row");
            } else {
                document.getElementById("memberSection")?.classList.add("row");
                document.getElementById("chatSection")?.classList.add("row");
            }
        }

        window.onbeforeunload =  () => {
            Script.Instance.webSocket.send({ type: ClientEvents.LEAVT_EVENT });
        }
        
        this.webSocket = new WebSocketClient("ws://localhost:2222");
        this.webSocket.recieve = this.onServerResponse;

        this.loginDiv = document.getElementById("loginPage") as HTMLDivElement;
        this.pageDiv = document.getElementById("viewPage") as HTMLDivElement;
        var loginButton: HTMLButtonElement = document.getElementById("loginButton") as HTMLButtonElement;
        var loginName: HTMLInputElement = document.getElementById("nameInput") as HTMLInputElement;
        var raumInput: HTMLInputElement = document.getElementById("roomInput") as HTMLInputElement;

        this.loginDiv.hidden = false;
        this.pageDiv.hidden = true;

        loginButton.onclick = () => {
            if(StringHelper.isNullOrEmpty(loginName.value)) {
                console.log("Kein Name");
                return;
            }

            if(StringHelper.isNullOrEmpty(raumInput.value) || Number.isInteger(raumInput.value)) {
                console.log("Kein Raum");
                return;
            }

            this.webSocket.send({ type: ClientEvents.LOGIN_EVENT, name: loginName.value, room: Number.parseInt(raumInput.value)});
            loginButton.disabled = true;
        }

        var messageBox: HTMLInputElement = document.getElementById("chatText") as HTMLInputElement;
        var sendButton: HTMLButtonElement = document.getElementById("sendButton") as HTMLButtonElement;
        var inputFile: HTMLInputElement = document.getElementById("inputGroupFile01") as HTMLInputElement;
        messageBox.onkeydown = (ev) => {
            if(StringHelper.isNullOrEmpty(messageBox.value)) {
                Script.Instance.webSocket.send({type:ClientEvents.TYPING_INDICATOR_CHANGED, status: TypingIndication.NOTHING});
            } else {
                Script.Instance.webSocket.send({type:ClientEvents.TYPING_INDICATOR_CHANGED, status: TypingIndication.WRITING});
            }
        }

        console.log(sendButton);
        sendButton.onclick = (ev) => {
            (document.getElementById("custom-file-label") as HTMLInputElement).innerText = "Choose File";

            if(!(inputFile.files && inputFile.files.length > 0)) {
                Script.Instance.webSocket.send({ type: ClientEvents.MESSAGE_SENT, message: { time: Date.now(), message: messageBox.value, name: loginName.value, dataUrl: "" }});
            } else {    
                imageCompression(inputFile.files[0], { maxSizeMB: 1, maxWidthOrHeight: 1080, useWebWorker: true }).then((success) => {
                    imageCompression.getDataUrlFromFile(success).then((baseDate) => {
                        Script.Instance.webSocket.send({ type: ClientEvents.MESSAGE_SENT, message: { time: Date.now(), message: messageBox.value, name: loginName.value, dataUrl: baseDate }});
                    });
                });
                
            }
            
            messageBox.value = "";
            inputFile.value = "";
        }

        inputFile.onchange = (ev) => {
            if(!(inputFile.files && inputFile.files.length > 0)) {
                (document.getElementById("custom-file-label") as HTMLInputElement).innerText = "Choose File";
                return;
            }

            const acceptedImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
            var type = inputFile.files[0];

            if(!acceptedImageTypes.some(x => x == type.type)) {
                inputFile.value = "";
                alert("This File is not a Image");
                return;
            }
            
            (document.getElementById("custom-file-label") as HTMLInputElement).innerText = inputFile.files[0].name;
        }
    }

    private onServerResponse(m: ServerMessage): void {
        console.log(m);
        
        if(m.type == ServerEvents.LOGIN_SUCCESFULL) {
            Script.Instance.loginDiv.hidden = true;
            Script.Instance.pageDiv.hidden = false;

            m.chatters.forEach((current) => {
                Script.Instance.addChatter(current.uuid, current.name);
            })
        } else if (m.type == ServerEvents.CHATTER_JOINED) {
            Script.Instance.addChatter(m.uuid, m.name);
        } else if (m.type == ServerEvents.CHATTER_LEAVT) {
            Script.Instance.removeChatter(m.uuid);
        } else if (m.type == ServerEvents.TYPING_INDICATOR) {
            Script.Instance.enableLoadingAnimation(m.uuid, m.status == TypingIndication.WRITING);
        } else if (m.type == ServerEvents.MESSAGE_SENT) {
            Script.Instance.recieveMessage(m.message);
        }
    }

    private addChatter(uuid: string, name: string): void {
        var memberHtml = this.MemberTemplate.replace("{{uuid}}", uuid);
        memberHtml = memberHtml.replace("{{name}}", name);

        document.getElementById("membersList")?.insertAdjacentHTML("beforeend", memberHtml);
    }

    private removeChatter(uuid: string): void {
        document.getElementById("member-" + uuid)?.remove();
    }

    private recieveMessage(message: MessageSentEntity): void {
        var messageHtml = this.MessageTemplate.replace("{{name}}", message.name);
        messageHtml = messageHtml.replace("{{message}}", message.message);
        messageHtml = messageHtml.replace("{{time}}", new Date(message.time).getHours() + ":" + new Date(message.time).getMinutes())

        if(!StringHelper.isNullOrEmpty(message.dataUrl)) {
            messageHtml = messageHtml.replace("{{image}}", this.ImageTemplate.replace("{{dataUrl}}", message.dataUrl));
        } else {
            messageHtml = messageHtml.replace("{{image}}", "");
        }

        document.getElementById("chatwindow")?.insertAdjacentHTML("beforeend", messageHtml);
        
        document.getElementById("chatwindow")?.scrollTo({ top:  document.getElementById("chatwindow")?.scrollHeight});
    }

    private enableLoadingAnimation(uuid: string, status: boolean): void {
        //TODO: Doesnt work
    }
}


new Script().start();