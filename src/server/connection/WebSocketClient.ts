import { ServerMessage } from "@shared/message/ServerMessage";
import { randomUUID } from "crypto";
import WebSocket from "ws";
import Client from "./Client";

export default class WebSocketClient implements Client {
	public readonly uuid: string;
	public name: string;

	public constructor(private socket: WebSocket, public readonly ip: string) {
		this.uuid = randomUUID();
	}

	public send(m: ServerMessage): void {
		this.socket.send(JSON.stringify(m));
	}

	public get isOpen(): boolean {
		return this.socket.readyState === WebSocket.OPEN;
	}
}