import Client from "@server/connection/Client";

export default class RaumCache {

    public static readonly Instance: RaumCache = new RaumCache();

    private rooms: [number, Client[]][] = [];

    public addClient(room: number, client: Client): void {
        var added = false;
        
        this.rooms.forEach((current) => {            
            if(current[0] == room) {
                current[1].push(client);
                added = true;
            }
        });

        if(!added) {
            this.rooms.push([room, [client]]);
        }
    }

    public removeClient(client: Client): void {
        var roomNumber = this.getRoomByClient(client);
        var filteredClients: Client[] = [];

        this.rooms.forEach((current) => {
            if(current[0] == roomNumber) {
                filteredClients = current[1].filter(x => x.uuid != client.uuid);
            }
        });

        var idx = this.rooms.findIndex((x) => x[0] == roomNumber);
        this.rooms = this.rooms.splice(idx, 1);

        if(filteredClients.length > 0) {
            this.rooms.push([roomNumber, filteredClients]);
        }
    }

    public getAnzClientsPerRoom(room: number): number {
        var clients = 0;

        this.rooms.forEach((current) => {
            if(current[0] == room) {
                clients = current[1].length;
            }
        });

        return clients;
    }

    public getClientsPerRoom(room: number): Client[] {
        var clients: Client[] = [];

        this.rooms.forEach((current) => {
            if(current[0] == room) {
                clients = current[1];
            }
        });  

        return clients;
    }

    public getRoomByClient(client: Client): number {
        var retVal = 0;

        this.rooms.forEach((current) => {
            if(current[1].find(x => x.uuid == client.uuid) != null) {
                retVal = current[0];
            }
        });
        
        return retVal;
    }
}