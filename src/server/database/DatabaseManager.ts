import { createConnection, Repository } from "typeorm";
import ChatMessage from "./entity/ChatMessage";
import ChatRoom from "./entity/ChatRooms";

export default class DatabaseManager {

    private static instance: DatabaseManager;

    public static getInstance(): DatabaseManager {
        if(DatabaseManager.instance == null) {
            DatabaseManager.instance = new DatabaseManager();
        }

         return DatabaseManager.instance;
    };

    public repoChatRooms: Repository<ChatRoom>;
    public repoChatMessages: Repository<ChatMessage>;

    private constructor() {

    }

    public async connectDatabase(): Promise<void>  {
        var connection = await createConnection({
            type: "mysql",
            host: "mysql",
            port: 3306,
            username: "root",
            password: "supersecret",
            database: "test",
            entities:  [ ChatMessage, ChatRoom ],
            synchronize: true,
        });

        this.repoChatRooms = connection.getRepository(ChatRoom);
        this.repoChatMessages = connection.getRepository(ChatMessage);
    }

}