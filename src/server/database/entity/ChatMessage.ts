import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import ChatRoom from "./ChatRooms";

@Entity()
export default class ChatMessage {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    sender: string;

    @Column()
    message: string;

    @Column({ type: "timestamp", nullable: true})
    sended?: Date;

    @ManyToOne(type => ChatRoom, chatrom => chatrom.messages)
    room: ChatRoom;

}