import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import ChatMessage from "./ChatMessage";

@Entity()
export default class ChatRoom {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    room: number;

    @OneToMany(() => ChatMessage, message => message.room)
    messages: ChatMessage[];
}