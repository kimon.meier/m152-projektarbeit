import ChatterInfo from "@shared/entities/ChatterInfo";
import { ClientEvents } from "@shared/enums/ClientEvents";
import { ServerEvents } from "@shared/enums/ServerEvents";
import { ClientMessage } from "@shared/message/ClientMessage";
import "reflect-metadata";
import WebSocketClient from "./connection/WebSocketClient";
import WebSocketConnection from "./connection/WebSocketConnection";
import RaumCache from "./core/RaumCache";
import DatabaseManager from "./database/DatabaseManager";
import ChatMessage from "./database/entity/ChatMessage";
import ChatRoom from "./database/entity/ChatRooms";

const server = new WebSocketConnection();


server.addListener("message", async (client: WebSocketClient, data: ClientMessage) => {
    console.log(data);

    if(data.type == ClientEvents.CONNECTION_SUCCESS) {
        console.log("Der Client " + client.ip + " hat sich erfolgreich verbunden!");
        
    } else if (data.type == ClientEvents.LOGIN_EVENT) {
        var chatterInfos: ChatterInfo[] = new Array();
        
        RaumCache.Instance.getClientsPerRoom(data.room).forEach((current) => {
            current.send({ type: ServerEvents.CHATTER_JOINED, name: data.name, uuid: client.uuid });
            chatterInfos.push({ name: current.name, uuid: current.uuid });
        })

        client.name = data.name;
        client.send({ type: ServerEvents.LOGIN_SUCCESFULL, chatters: chatterInfos, uuid: client.uuid });

        RaumCache.Instance.addClient(data.room, client);

        if((await DatabaseManager.getInstance().repoChatRooms.findAndCount({ where: { room: data.room } }))[1] == 0) {
            var dbRoom = new ChatRoom();
            dbRoom.room = data.room;

            await DatabaseManager.getInstance().repoChatRooms.save(dbRoom);
        };

    } else if (data.type == ClientEvents.LEAVT_EVENT) {
        var room = RaumCache.Instance.getRoomByClient(client); 

        RaumCache.Instance.removeClient(client);

        RaumCache.Instance.getClientsPerRoom(room).forEach((current) => {
            current.send({ type: ServerEvents.CHATTER_LEAVT, uuid: client.uuid});
        });

    } else if (data.type == ClientEvents.TYPING_INDICATOR_CHANGED) {
        var room = RaumCache.Instance.getRoomByClient(client);

        RaumCache.Instance.getClientsPerRoom(room).forEach((current) => {
            console.log(current.uuid);
            
            if(current.uuid == client.uuid) {
                return;
            }
            current.send({ type: ServerEvents.TYPING_INDICATOR, status: data.status, uuid: client.uuid});
        });
        
    } else if (data.type == ClientEvents.MESSAGE_SENT) {
        var room = RaumCache.Instance.getRoomByClient(client);
        
        RaumCache.Instance.getClientsPerRoom(room).forEach((current) => {
            current.send({ type: ServerEvents.MESSAGE_SENT, message: data.message });
        });
        
        var dbRoom: ChatRoom = await DatabaseManager.getInstance().repoChatRooms.findOneOrFail({ where: { room: room }});
        var msgToSafe = new ChatMessage();
        msgToSafe.message = data.message.message;
        msgToSafe.room = dbRoom;
        msgToSafe.sended = new Date(Date.now());
        msgToSafe.sender = data.message.name;
        DatabaseManager.getInstance().repoChatMessages.save(msgToSafe);
    }
    
});

setTimeout(() => {
    DatabaseManager.getInstance().connectDatabase();
}, 5 * 1000)

server.connect();
