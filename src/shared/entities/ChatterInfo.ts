export default interface ChatterInfo {
    name: string;
    uuid: string;
}