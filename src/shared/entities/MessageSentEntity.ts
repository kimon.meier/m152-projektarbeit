
export default interface MessageSentEntity {
    name: string,
    time: number,
    message: string,
    dataUrl: string;
}