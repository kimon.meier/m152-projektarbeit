/// These Events are executed on the Client
export enum ClientEvents {
    /// On this Event the Client is connected to the Server and got an UUID
    CONNECTION_SUCCESS,

    /// ON this Evene the Client requests to login
    LOGIN_EVENT,

    /// On this Event the Client disconnects
    LEAVT_EVENT,

    /// Om this Event the Client sends a Chatmessage to the Server
    MESSAGE_SENT,

    /// On this Event the Client sends a Typing Indicator to the Server
    TYPING_INDICATOR_CHANGED,

}