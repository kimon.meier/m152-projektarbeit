/// These Events are executed on the Server
export enum ServerEvents {
    PING,

    /// On this Event the Servers sends the Client the Registrationapproval
    LOGIN_SUCCESFULL,

    /// On this Event a new Chatter joined the Chat
    CHATTER_JOINED,

    /// On this Event a Chatter leavt the Chat
    CHATTER_LEAVT,

    /// On this Event a Message was sent in the Chat
    MESSAGE_SENT,

    /// On this Event a Chatter updated their typing indicator
    TYPING_INDICATOR
}