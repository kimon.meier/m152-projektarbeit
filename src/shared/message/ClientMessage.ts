import MessageSentEntity from "@shared/entities/MessageSentEntity";
import TypingIndication from "@shared/entities/TypingIndicator";
import { ClientEvents } from "@shared/enums/ClientEvents";

interface ConnectionSuccessEvent {
    type: ClientEvents.CONNECTION_SUCCESS;
    uuid: string;
}

interface LoginEvent {
    type: ClientEvents.LOGIN_EVENT;
    name: string;
    room: number;
}

interface LeavtEvent {
    type: ClientEvents.LEAVT_EVENT;
}

interface MessageSentEvent {
    type: ClientEvents.MESSAGE_SENT,
    message: MessageSentEntity
}

interface TypingIndicatorChangedEvent {
    type: ClientEvents.TYPING_INDICATOR_CHANGED,
    status: TypingIndication;
}

export type ClientMessage = ConnectionSuccessEvent | LoginEvent | LeavtEvent | MessageSentEvent | TypingIndicatorChangedEvent;