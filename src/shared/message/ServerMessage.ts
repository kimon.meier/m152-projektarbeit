import ChatterInfo from "@shared/entities/ChatterInfo";
import MessageSentEntity from "@shared/entities/MessageSentEntity";
import TypingIndication from "@shared/entities/TypingIndicator";
import { ServerEvents } from "@shared/enums/ServerEvents";

interface PingServerEvent {
    type: ServerEvents.PING;
    time: number;
}

interface LoginSuccesfullEvent {
    type: ServerEvents.LOGIN_SUCCESFULL;
    chatters: ChatterInfo[];
    uuid: string;
}

interface ChatterJoinedEvent {
    type: ServerEvents.CHATTER_JOINED;
    name: string;
    uuid: string;
}

interface ChatterLeavtEvent {
    type: ServerEvents.CHATTER_LEAVT;
    uuid: string;
}

interface TypingIndicatorChangedEvent {
    type: ServerEvents.TYPING_INDICATOR;
    status: TypingIndication;
    uuid: string;
}

interface MessageSentEvent {
    type: ServerEvents.MESSAGE_SENT;
    message: MessageSentEntity;
}

export type ServerMessage = PingServerEvent | LoginSuccesfullEvent | ChatterJoinedEvent | ChatterLeavtEvent | TypingIndicatorChangedEvent | MessageSentEvent;